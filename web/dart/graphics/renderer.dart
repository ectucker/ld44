part of ld44;

class Renderer {
  GLWrapper gl;

  WorldMap world;

  PlayerController controller;

  num screenHeight;
  num screenWidth;

  int worldWidth;
  int worldHeight;

  Framebuffer worldFrame;
  MapCamera worldCamera;
  SpriteBatch worldBatch;

  Camera2D screenCamera;
  SpriteBatch screenBatch;

  SpriteBatch uiBatch;

  BitmapFont font;
  SpriteBatch textBatch;

  double waveIntensity = 0.0;
  double waveAngle = 0.0;

  double scale = 1.0;

  Renderer(this.gl, this.world, this.screenWidth, this.screenHeight,
      assetManager, this.controller) {
    worldBatch = new SpriteBatch.defaultShader(gl, maxSprites: 5000);
    screenBatch = new SpriteBatch(gl, assetManager.get("insanity"));
    screenBatch.setAdditionalUniforms = () {
      screenBatch.setUniform(
          "waveData", new Vector2(waveAngle, 0.5 * pow(hotbar.insanity, 1.5)));
      screenBatch.setUniform(
          "uLightColor",
          Vector4(
              min(1 / (0.3 * hotbar.insanity), 1.0),
              min(1 / (0.3 * hotbar.insanity), 1.0),
              min(1 / (0.3 * hotbar.insanity), 1.0),
              1.0));
    };
    uiBatch = new SpriteBatch.defaultShader(gl);
    textBatch = new SpriteBatch(gl, assetManager.get("sdf"));
    font = assetManager.get("scroll");
    resize(screenWidth, screenHeight);
  }

  update(double delta) {
    waveAngle += delta * 5.0;
    while (waveAngle > pi * 2) waveAngle -= pi * 2;
  }

  drawBackground() {
    gl.clearScreen(Colors.midnightBlue);
    gl.context.enable(WebGL.BLEND);
    gl.context.blendFuncSeparate(WebGL.SRC_ALPHA, WebGL.ONE_MINUS_SRC_ALPHA,
        WebGL.ONE, WebGL.ONE_MINUS_SRC_ALPHA);
  }

  drawWorld(WorldMap world) {
    worldCamera.update();
    worldBatch.projection = worldCamera.combined;

    worldFrame.beginCapture();
    gl.setGLViewport(worldWidth, worldHeight);
    worldBatch.begin();

    gl.clearScreen(Colors.midnightBlue);

    for(int i = 1; i <= 3; i++) {
      worldBatch.draw(atlas["back$i"], worldCamera.x * backgroundMoveRates[i], 0);
      worldBatch.draw(atlas["back$i"], worldCamera.x * backgroundMoveRates[i] + atlas["back$i"].width, 0);
      worldBatch.draw(atlas["back$i"], worldCamera.x * backgroundMoveRates[i] + atlas["back$i"].width * 2, 0);
    }

    for (MapLayer layer in world.tilemap.layers) {
      if (!layer.name.contains("Foreground")) {
        layer.render(worldBatch, 0, 0);
      }
    }

    for (WorldEntity entity in world.entities) {
      entity.aura.draw(worldBatch);
    }

    worldBatch.color = Colors.white;

    for (WorldEntity entity in world.entities) {
      Vector4 color = entity.tint.clone();
      if (entity.vanished) {
        color.a = 0.2;
      }
      worldBatch.color = color;
      drawEntity(entity);
    }
    worldBatch.color = Colors.white;

    for (MapLayer layer in world.tilemap.layers) {
      if (layer.name.contains("Foreground")) {
        layer.render(worldBatch, 0, 0);
      }
    }

    worldBatch.end();
    worldFrame.endCapture();

    gl.setGLViewport(screenWidth, screenHeight);

    gl.clearScreen(Colors.black);

    screenCamera.update();
    screenBatch.projection = screenCamera.combined;
    screenBatch.begin();
    screenBatch.draw(worldFrame.texture, 0, 0,
        width: screenWidth, height: screenHeight);
    screenBatch.draw(worldFrame.texture, screenWidth, 0,
        width: screenWidth, height: screenHeight, flipX: true);
    screenBatch.draw(worldFrame.texture, -screenWidth, 0,
        width: screenWidth, height: screenHeight, flipX: true);
    screenBatch.draw(worldFrame.texture, 0, screenHeight,
        width: screenWidth, height: screenHeight, flipY: true);
    screenBatch.draw(worldFrame.texture, 0, -screenHeight,
        width: screenWidth, height: screenHeight, flipY: true);
    screenBatch.draw(worldFrame.texture, screenWidth, screenHeight,
        width: screenWidth, height: screenHeight, flipX: true, flipY: true);
    screenBatch.draw(worldFrame.texture, screenWidth, -screenHeight,
        width: screenWidth, height: screenHeight, flipY: true, flipX: true);
    screenBatch.draw(worldFrame.texture, -screenWidth, screenHeight,
        width: screenWidth, height: screenHeight, flipX: true, flipY: true);
    screenBatch.draw(worldFrame.texture, -screenWidth, -screenHeight,
        width: screenWidth, height: screenHeight, flipY: true, flipX: true);
    screenBatch.end();

    uiBatch.projection = screenBatch.projection;
    uiBatch.color = new Vector4(1.0, 1.0, 1.0, 0.5);
    uiBatch.begin();
    drawHotbar(hotbar, uiBatch);
    uiBatch.end();

    textBatch.projection = screenCamera.combined;
    textBatch.setAdditionalUniforms = () {
          gl.context
              .uniform1f(textBatch.shaderProgram.uniforms["uSdfScale"], scale);
        };
    textBatch.begin();
    textBatch.texture = font.texture;
    textBatch.color = new Vector4(1.0, 1.0, 1.0, 0.5);
    if (world.currentShrine != null) {
      int drawX = ((world.currentShrine.tileX * TILE_SIZE +
                      TILE_SIZE / 2 -
                      worldCamera.x) *
                  scale -
              screenWidth)
          .toInt();
      font.drawParagraph(
          textBatch,
          drawX,
          ((world.currentShrine.tileY + 1.5) * TILE_SIZE * scale).toInt(),
          tomes[world.currentShrine.type],
          lineWidth: screenWidth,
          align: TextAlign.center,
          scale: scale);
    }
    font.drawParagraph(
        textBatch,
        (screenWidth / 2 + TILE_SIZE * -4 * scale).toInt(),
        (TILE_SIZE * 1.2 * scale).toInt(),
        "Q",
        lineWidth: (TILE_SIZE * scale).toInt(),
        scale: scale,
        align: TextAlign.center);
    font.drawParagraph(
        textBatch,
        (screenWidth / 2 + TILE_SIZE * -1 * scale).toInt(),
        (TILE_SIZE * 1.2 * scale).toInt(),
        "W",
        lineWidth: (TILE_SIZE * scale).toInt(),
        scale: scale,
        align: TextAlign.center);
    font.drawParagraph(
        textBatch,
        (screenWidth / 2 + TILE_SIZE * 2 * scale).toInt(),
        (TILE_SIZE * 1.2 * scale).toInt(),
        "E",
        lineWidth: (TILE_SIZE * scale).toInt(),
        scale: scale,
        align: TextAlign.center);
    for (WorldEntity e in world.entities) {
      if (e is TutorialEntity) {
        int drawX = (((e.x - 6) * TILE_SIZE - worldCamera.x) * scale).toInt();
        font.drawWord(
            textBatch, drawX, ((e.y - 2) * TILE_SIZE * scale).toInt(), e.text,
            scale: scale);
      }
    }
    if (!game.started) {
      textBatch.color = Colors.white;
      font.drawParagraph(textBatch, -screenWidth ~/ 2, screenHeight ~/ 2, "Press N to Start",
          lineWidth: screenWidth, scale: scale, align: TextAlign.center);
    }
    textBatch.end();
  }

  drawEntity(WorldEntity entity) {
    worldBatch.draw(
        entity.texture, (entity.smoothX) * TILE_SIZE + entity.drawOffsetX, (entity.smoothY ) * TILE_SIZE  + entity.drawOffsetY,
        flipX: entity.facing == -1);
  }

  drawHotbar(Hotbar bar, SpriteBatch batch) {
    batch.draw(bar.q.icon, screenWidth / 2 + TILE_SIZE * -4 * scale,
        TILE_SIZE * 1.5 * scale,
        scaleX: scale, scaleY: scale);
    batch.draw(bar.w.icon, screenWidth / 2 + TILE_SIZE * -1 * scale,
        TILE_SIZE * 1.5 * scale,
        scaleX: scale, scaleY: scale);
    batch.draw(bar.e.icon, screenWidth / 2 + TILE_SIZE * 2 * scale,
        TILE_SIZE * 1.5 * scale,
        scaleX: scale, scaleY: scale);
    for (int i = 1; i <= world.player.health; i++) {
      batch.draw(atlas["heart"],
          screenWidth / 2 + TILE_SIZE * (3 + 2 * i) * scale, TILE_SIZE * scale,
          scaleX: scale, scaleY: scale);
    }
    for (int i = 1; i <= hotbar.insanity; i++) {
      batch.draw(
          atlas["insanity"],
          screenWidth / 2 + TILE_SIZE * (-5 + -2 * i) * scale,
          TILE_SIZE * scale,
          scaleX: scale,
          scaleY: scale);
    }
  }

  resize(num width, num height) {
    this.screenWidth = width;
    this.screenHeight = height;

    double aspectRatio = width / height;
    worldWidth = (SCREEN_PIXEL_HEIGHT * aspectRatio).ceil();
    worldHeight = SCREEN_PIXEL_HEIGHT;

    scale = screenHeight / worldHeight;

    worldFrame = new Framebuffer(gl, worldWidth, worldHeight);
    worldCamera = new MapCamera(world, worldWidth, worldHeight);

    screenCamera = new Camera2D.originBottomLeft(screenWidth, screenHeight);

    gl.setGLViewport(screenWidth, screenHeight);
  }

  dragCamera(WorldEntity player, double delta) {
    try {
      double newX = player.smoothX * TILE_SIZE - worldWidth / 2 - TILE_SIZE;
      if (newX != null) {
        worldCamera.x = worldCamera.x + 5 * ((newX - worldCamera.x) * delta);
      }
    } catch (e) {}
  }
}
