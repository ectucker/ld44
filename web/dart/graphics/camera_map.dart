part of ld44;

class MapCamera {

  num width;
  num height;

  WorldMap map;
  Camera2D camera;

  MapCamera(this.map, this.width, this.height) {
    camera = new Camera2D.originBottomLeft(width, height);
  }

  num constrained(num x, num min, num max) {
    if(min < x && x < max) {
      return x;
    }
    if(x < min) {
      return min;
    }
    if(x > max) {
      return max;
    }
  }

  set x(x) {
    camera.x = constrained(x, 0, map.width * TILE_SIZE - width);
  }

  set y(y) {
    camera.y = constrained(y, 0, map.height * TILE_SIZE - height);
  }

  get x => camera.x;
  get y => camera.y;

  get combined => camera.combined;

  update() {
    camera.update();
  }

}