part of ld44;

class Victory extends State {
  Game game;

  BitmapFont font;

  SpriteBatch textBatch;
  Camera2D screenCamera;

  double scale;

  Victory(this.game);

  @override
  create() {
    tweenManager = this.tweenManager;

    textBatch = new SpriteBatch(gl, assetManager.get("sdf"));
    font = assetManager.get("scroll");
    resize(width, height);
  }

  @override
  render(num delta) {
    gl.clearScreen(Colors.black);
    textBatch.projection = screenCamera.combined;
    textBatch.setAdditionalUniforms = () {
          gl.context
              .uniform1f(textBatch.shaderProgram.uniforms["uSdfScale"], scale);
        };
    ;
    textBatch.begin();
    textBatch.texture = font.texture;
    font.drawParagraph(textBatch, -width ~/ 2, height ~/ 2, "YOU ESCAPE",
        lineWidth: width, align: TextAlign.center, scale: scale);
    font.drawParagraph(
        textBatch, -width ~/ 2, height ~/ 4, "But at the cost of ${hotbar.insanity} sanity",
        lineWidth: width, align: TextAlign.center, scale: scale);
    textBatch.end();
  }

  @override
  update(num delta) {
    if (keyboard.keyPressed(KeyCode.R)) {
      game.restartLevel();
    }
    if (keyboard.keyPressed(KeyCode.N)) {
      game.restartGame();
    }
  }

  @override
  resize(num width, num height) {
    screenCamera = new Camera2D.originBottomLeft(width, height);
    scale = SCREEN_PIXEL_HEIGHT / height;
  }

  @override
  pause() {
    // TODO: implement pause
    return null;
  }

  @override
  resume() {
    // TODO: implement resume
    return null;
  }

  @override
  preload() {
    // TODO: implement preload
    return null;
  }
}
