part of ld44;

class Game extends BaseGame {
  State currentState;

  int currentLevel = 0;

  Camera2D screenCamera;
  SpriteBatch transitionBatch;

  int transNum;
  double transitionAmount = 1.0;

  Random rand = new Random();

  num targetMusicTime = 0;
  int currentMusicNum = 0;
  Music currentMusic;

  bool started = false;

  Game() {
    currentState = new GameState(this, currentLevel);
    randomizeTrans();
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

  @override
  create() {
    transitionBatch = new SpriteBatch(gl, assetManager.get("transition"));
    transitionBatch.setAdditionalUniforms = () {
      gl.context.uniform1f(transitionBatch.shaderProgram.uniforms["uPercent"], transitionAmount);
    };
    currentState.create();
    resize(width, height);
    Tween()
      ..get = [() => this.transitionAmount]
      ..set = [(p) => this.transitionAmount = p]
      ..target = [0.0]
      ..duration = 0.5
      ..start(tweenManager);
    currentMusic = assetManager.get("0_Insanity");
  }

  @override
  preload() {
    assetManager = this.assetManager;
    Future atlasFuture =
        loadAtlas("img/atlas.json", loadTexture(gl, "img/atlas.png", linear));
    assetManager.load("atlas", atlasFuture);
    assetManager.load(
        "scroll",
        loadFont("fnt/scrollfont.fnt",
            loadTexture(gl, "fnt/scrollfont.png", linear)));
    assetManager.load(
        "sdf", loadProgram(gl, "glsl/default.vert", "glsl/sdf.frag"));
    assetManager.load(
        "insanity", loadProgram(gl, "glsl/ripple.vert", "glsl/tint.frag"));
    assetManager.load("transition",
        loadProgram(gl, "glsl/default.vert", "glsl/overlay.frag"));
    assetManager.load(
        "playeraura",
        loadEffect("data/playeraura.json",
            atlasFuture.then((atlas) => atlas["particle"])));
    assetManager.load(
        "enemyaura",
        loadEffect("data/enemyaura.json",
            atlasFuture.then((atlas) => atlas["particle"])));
    assetManager.load(
        "bookaura",
        loadEffect("data/enemyaura.json",
            atlasFuture.then((atlas) => atlas["particle"])));
    assetManager.load(
        "noaura",
        loadEffect("data/noaura.json",
            atlasFuture.then((atlas) => atlas["particle"])));
    for (int i = 0; i <= LAST_LEVEL; i++) {
      assetManager.load("level$i", loadTilemap("map/level$i.tmx"));
    }
    for (int i = 0; i <= 3; i++) {
      assetManager.load("${i}_Insanity", loadMusic(audio, "snd/${i}_Insanity.ogg"));
    }
    assetManager.load("switch", loadSound(audio, "snd/Lever.ogg"));
    assetManager.load("powerup", loadSound(audio, "snd/Powerup.ogg"));
    assetManager.load("swing", loadSound(audio, "snd/Sword_Swing.ogg"));
    assetManager.load("hit", loadSound(audio, "snd/Sword_Hit.ogg"));
    assetManager.load("walk", loadSound(audio, "snd/Walking.ogg"));
    assetManager.load("climb", loadSound(audio, "snd/Climb.ogg"));
    assetManager.load("enemyattack", loadSound(audio, "snd/Enemy_Attack.ogg"));
  }

  @override
  render(num delta) {
    currentState.render(delta);
    transitionBatch.projection = screenCamera.combined;
    transitionBatch.begin();
    transitionBatch.draw(atlas["transition$transNum"], 0, 0,
        height: height, width: (atlas["transition$transNum"].height / height) * width);
    transitionBatch.draw(atlas["transition$transNum"], (atlas["transition$transNum"].height / height) * width, 0,
        height: height, width: (atlas["transition$transNum"].height / height) * width);
    transitionBatch.end();
  }

  @override
  update(num delta) {
    if(started) {
      currentState.update(delta);
      if (currentMusicNum != insanityMusicNum(hotbar.insanity)) {
        num time = currentMusic.time;
        currentMusic.stop();
        currentMusicNum = insanityMusicNum(hotbar.insanity);
        currentMusic = assetManager.get("${currentMusicNum}_Insanity");
        currentMusic.loop();
        currentMusic.element.currentTime = time;
      }
      if (currentMusic.time < targetMusicTime) {
        currentMusic.element.currentTime = targetMusicTime;
      }
    } else if(!started && keyboard.keyJustPressed(KeyCode.N)) {
      started = true;
      currentMusic.loop();
    }
  }
  
  int insanityMusicNum(int insanity) {
    if(insanity >= 6) {
      return 3;
    }
    if(insanity >= 3) {
      return 2;
    }
    if(insanity >= 1) {
      return 1;
    }
    return 0;
  }

  @override
  resume() {}

  @override
  resize(num width, num height) {
    currentState.resize(width, height);
    screenCamera = new Camera2D.originBottomLeft(width, height);
  }

  nextLevel() {
    Game game = this;
    transition(() {
      if(currentLevel != LAST_LEVEL) {
        oldHotbar = hotbar.clone();
        currentLevel++;
        currentState = new GameState(game, currentLevel);
        currentState.create();
      } else {
        currentState = new Victory(game);
        currentState.create();
      }
    });
  }

  restartLevel() {
    Game game = this;
    transition(() {
      hotbar = oldHotbar;
      currentState = new GameState(game, currentLevel);
      currentState.create();
    });
  }

  restartGame() {
    Game game = this;
    transition(() {
      hotbar = new Hotbar(atlas);
      oldHotbar = new Hotbar(atlas);
      currentLevel = 0;
      currentState = new GameState(game, currentLevel);
      currentState.create();
    });
  }

  gameOver() {
    Game game = this;
    transition(() {
      currentState = new Gameover(game);
      currentState.create();
    });
  }

  transition(Function onMid) {
    Tween()
      ..get = [() => this.transitionAmount]
      ..set = [(p) => this.transitionAmount = p]
      ..target = [1.0]
      ..duration = 0.5
      ..callback = onMid
      ..chain(Tween()
        ..get = [() => this.transitionAmount]
        ..set = [(p) => this.transitionAmount = p]
        ..target = [0.0]
        ..callback = () {
          this.transitionAmount = 0.0;
        }
        ..duration = 0.5)
      ..start(tweenManager);
  }

  randomizeTrans() {
    transNum = rand.nextInt(4) + 1;
  }
}
