library ld44;

import 'package:cobblestone/cobblestone.dart';
import 'dart:web_gl';

import 'package:xml/xml.dart' as XML;

part 'game.dart';

part 'map/map.dart';
part 'entity/entity.dart';
part 'entity/player_controller.dart';
part 'graphics/renderer.dart';
part 'graphics/camera_map.dart';
part 'map/tilemap.dart';
part 'map/tile.dart';
part 'map/switch.dart';
part 'map/shrine.dart';
part 'entity/powerup.dart';
part 'entity/enemy_controller.dart';
part 'platformer/game_screen.dart';
part 'entity/tutorial.dart';
part 'ui/gameover.dart';
part 'ui/victory.dart';

part 'consts.dart';

var atlas;
var tweenManager;
var game;
var assetManager;

void main() {
  game = new Game();
}
