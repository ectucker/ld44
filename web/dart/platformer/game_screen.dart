part of ld44;

class GameState extends State {

  int level;

  Game game;

  WorldMap world;

  PlayerController controller;

  Renderer renderer;

  GameState(this.game, this.level);

  @override
  create() {
    tweenManager = this.tweenManager;

    atlas = assetManager.get("atlas");
    assetManager = this.assetManager;

    Tilemap map = assetManager.get("level$level");
    map.giveTileset(atlas["tileset"]);
    world = new WorldMap(map);

    WorldEntity entity = new WorldEntity(world, atlas["player"], startPos[level][0], startPos[level][1], PLAYER_WIDTH, PLAYER_HEIGHT, assetManager.get("playeraura"));
    world.addEntity(entity);
    world.player = entity;

    controller = new PlayerController(entity, world, keyboard, mouse, atlas);

    renderer = new Renderer(gl, world, width, height, assetManager, controller);
  }

  @override
  render(num delta) {
    renderer.drawBackground();
    renderer.drawWorld(world);
  }

  @override
  update(num delta) {
    controller.update(delta);
    if(controller.player.y == 0) {
      controller.player.health = 0;
    }
    if(controller.player.health <= 0) {
      controller.controlled = false;
      game.gameOver();
    }
    renderer.dragCamera(controller.player, delta);
    world.update(delta);
    renderer.update(delta);
  }

  @override
  resize(num width, num height) {
    renderer.resize(width, height);
  }

  @override
  pause() {
    // TODO: implement pause
    return null;
  }

  @override
  resume() {
    // TODO: implement resume
    return null;
  }

  @override
  preload() {
    // TODO: implement preload
    return null;
  }

}