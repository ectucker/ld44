part of ld44;

List<Shrine> listShrines(DataLayer layer) {
  List<Shrine> shrines = [];
  layer.layer.findElements("object").forEach((XML.XmlElement object) {
    int x = int.parse(object.getAttribute("x")) ~/ TILE_SIZE;
    int y = (SCREEN_PIXEL_HEIGHT - TILE_SIZE - int.parse(object.getAttribute("y"))) ~/ TILE_SIZE;
    shrines.add(new Shrine(object.getAttribute("name"), x, y));
  });
  return shrines;
}

class Shrine {

  int tileX;
  int tileY;

  String type;

  Shrine(this.type, this.tileX, this.tileY);

  bool checkIntersect(WorldEntity player) {
    return (player.x == tileX || player.xRight == tileX) && player.y == tileY;
  }

  @override
  String toString() {
    return "$tileX,$tileY,$type";
  }

}