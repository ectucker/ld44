part of ld44;

/// A tile on the map
class Tile {

  int id;

  Texture texture;

  Tile(this.texture, this.id);

  render(SpriteBatch batch, num x, num y, num width, num height) {
    batch.draw(texture, x, y, width: width, height: height);
  }

}