part of ld44;

class WorldMap {

  Tilemap tilemap;

  WorldEntity player;

  TileLayer physics;
  List<Switch> switches;
  List<Shrine> shrines;

  int get width => tilemap.width;
  int get height => tilemap.height;

  List<WorldEntity> entities;

  Shrine currentShrine;

  WorldMap(this.tilemap) {
    entities = [];
    physics = tilemap.getLayersContaining("Physics").first;
    switches = [];
    if(tilemap.getLayersContaining("Switch").isNotEmpty) {
      tilemap.getLayersContaining("Switch").forEach((MapLayer switchLayer) =>
          switches.add(new Switch(tilemap, switchLayer, physics)));
    }
    shrines = [];
    if(tilemap.getLayersContaining("Shrines").isNotEmpty) {
      tilemap.getLayersContaining("Shrines").forEach((MapLayer shrineLayer) =>
          shrines.addAll(listShrines(shrineLayer)));
    }
    for(Shrine shrine in shrines) {
      //entities.add(new WorldEntity(this, atlas["blank"], shrine.tileX, shrine.tileX, 1, 1, assetManager.get("bookaura")));
      //print("add shrine aura");
    }
    if(tilemap.getLayersContaining("Enemies").isNotEmpty) {
      tilemap.getLayersContaining("Enemies").forEach((MapLayer enemyLayer) =>
          entities.addAll(listEnemies(enemyLayer, this)));
    }
    if(tilemap.getLayersContaining("Tutorial").isNotEmpty) {
      tilemap.getLayersContaining("Tutorial").forEach((MapLayer tutLayer) =>
          entities.addAll(listTexts(tutLayer, this)));
    }
  }

  update(double delta) {
    for(Switch flip in switches) {
      if(flip.checkFlip(player.x, player.y)) {
        flip.flip();
      }
    }

    currentShrine = null;
    for(Shrine shrine in shrines) {
      if(shrine.checkIntersect(player)) {
        currentShrine = shrine;
      }
    }

    List<WorldEntity> remove = [];
    for(WorldEntity e in entities) {
      e.update(delta);
      if(e is Enemy) {
        e.player = player;
      }
      if(e.health <= 0) {
        remove.add(e);
      }
    }
    for(WorldEntity e in remove) {
      if(e is Enemy) {
        entities.remove(e);
      }
    }
  }

  addEntity(WorldEntity entity) {
    entities.add(entity);
  }

  bool isSolid(int x, int y) {
    if(x < 0 || x > width - 1) {
      return true;
    }
    if(y < 0 || y > height - 1) {
      return true;
    }
    return physics.getTile(x, y) != null;
  }

  int gapDist(int x, int y, int direction) {
    int num = direction;
    while(!isSolid(x + num, y - 1)) {
      num += direction;
    }
    return num - num.sign;
  }

  int platformHeight(int x, int y, int direction) {
    int height = 0;
    bool foundSolid = false;
    while(!foundSolid) {
      foundSolid = isSolid(x + direction, y + height);
      height += 1;
      if(height > 20) {
        return 0;
      }
    }
    bool foundAir = false;
    while(!foundAir) {
      foundAir = !isSolid(x + direction, y + height);
      height += 1;
      if(height > 20) {
        return 0;
      }
    }
    return height - 1;
  }

  int pitDepth(int x, int y, int direction) {
    int depth = 0;
    bool foundSolid = false;
    while(!foundSolid) {
      foundSolid = isSolid(x + direction, y + depth);
      depth -= 1;
      if(depth < -20) {
        return 0;
      }
    }
    depth += 2;
    if(depth > 0) {
      depth = 0;
    }
    return depth;
  }

  int firstTileAbove(int x, int y) {
    int height = 0;
    while(!isSolid(x, y + height)) {
      height += 1;
    }
    return height;
  }

}