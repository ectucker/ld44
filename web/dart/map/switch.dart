part of ld44;

class Switch {

  Tilemap map;

  TileLayer switchLayer;
  TileLayer physics;

  bool down = false;

  int xSwitch = 1000;
  int ySwitch;

  List<Vector2> switchTiles = [];
  List<Vector2> offTiles = [];
  List<Vector2> onTiles = [];

  Switch(this.map, this.switchLayer, this.physics) {
    for (int i = 0; i < switchLayer.width; i++) {
      for (int j = 0; j < switchLayer.height; j++) {
        Tile tile = switchLayer.getTile(i, j);
        if (tile != null) {
          switch (tile.id) {
            case SWITCH_ID:
              switchTiles.add(Vector2(i.toDouble(), j.toDouble()));
              break;
            case DEFAULT_OFF_ID:
              offTiles.add(Vector2(i.toDouble(), j.toDouble()));
              break;
            case DEFAULT_ON_ID:
              onTiles.add(Vector2(i.toDouble(), j.toDouble()));
              break;
          }
        }
      }
    }

    for(Vector2 tile in switchTiles) {
      if(tile.x < xSwitch) {
        xSwitch = tile.x.toInt() - 1;
        ySwitch = tile.y.toInt();
      }
    }

    print(xSwitch);
  }

  checkFlip(int playerX, int playerY) {
    if(down) {
      return false;
    }
    if(xSwitch == playerX && ySwitch == playerY - 1) {
      return true;
    }
    return false;
  }

  flip() {
    if(!down) {
      for (Vector2 on in offTiles) {
        physics.setTile(on.x.toInt(), on.y.toInt(), map.tileset[DEFAULT_ON_ID]);
        switchLayer.setTile(
            on.x.toInt(), on.y.toInt(), map.tileset[DEFAULT_ON_ID]);
      }
      for (Vector2 off in onTiles) {
        physics.setTile(off.x.toInt(), off.y.toInt(), null);
        switchLayer.setTile(
            off.x.toInt(), off.y.toInt(), map.tileset[DEFAULT_OFF_ID]);
      }
      var temp = offTiles;
      offTiles = onTiles;
      onTiles = temp;
      down = true;
      assetManager.get("switch").play();
    }
  }

}