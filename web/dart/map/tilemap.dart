part of ld44;

/// Loads a TMX tilemap from a URL. Use Tiled to create these. (http://www.mapeditor.org/)
Future<Tilemap> loadTilemap(String url) {
  return HttpRequest
      .getString(url)
      .then((String file) => new Tilemap(XML.parse(file)));
}

List<int> parseCsv(String csv) {
  List<int> parsed = [];
  csv.split(",").forEach((String tile) => parsed.add(int.parse(tile)));
  return parsed;
}

/// A 2D orthographic tilemap
class Tilemap {
  XML.XmlDocument file;

  List<MapLayer> layers;

  int width, height;
  int tileWidth, tileHeight;

  Map<int, BasicTile> basicTiles;
  Map<int, Tile> tileset;

  /// Creates a new tilemap from a TMX map
  Tilemap(this.file) {
    this.width = int.parse(file.rootElement.getAttribute("width"));
    this.height = int.parse(file.rootElement.getAttribute("height"));

    this.tileWidth = int.parse(file.rootElement.getAttribute("tilewidth"));
    this.tileHeight = int.parse(file.rootElement.getAttribute("tileheight"));

    layers = [];
    for (XML.XmlElement layer in file.rootElement.findElements("layer")) {
      layers.add(new TileLayer(layer, this));
    }
    for (XML.XmlElement layer in file.rootElement.findElements("objectgroup")) {
      layers.add(new DataLayer(layer, this));
    }
    //layers = layers.reversed.toList();
  }

  /// Gives the tilemap the textures it needs to render
  giveTileset(Texture set) {
    Map<int, Texture> tiles = set.split(TILE_SIZE, TILE_SIZE).asMap();

    tileset = Map.fromIterable(tiles.keys, key: (i) => i, value: (i) => new Tile(tiles[i], i));

    layers.forEach((MapLayer layer) => layer.giveTileset(tileset));
  }

  /// Renders the tilemap layers. If [filter] is set, only renders the layers with names selected by the it.
  render(SpriteBatch batch, num x, num y, {Pattern filter: null}) {
    if (filter == null) {
      for (MapLayer layer in layers) {
        layer.render(batch, x, y);
      }
    } else {
      for (MapLayer layer in getLayersContaining(filter)) {
        layer.render(batch, x, y);
      }
    }
  }

  /// Returns a list of map layers with names satisfying [filter]
  Iterable<MapLayer> getLayersContaining(Pattern filter) {
    return layers.where((MapLayer layer) => layer.name.contains(filter));
  }
}

/// A renderable layer of the map
abstract class MapLayer {
  String name;

  render(SpriteBatch batch, num x, num y);

  giveTileset(Map<int, Tile> tileset);
}

/// A tilemap layer filed with tiles
class TileLayer extends MapLayer {
  Tilemap parent;

  XML.XmlElement layer;

  List<Tile> tiles;
  List<int> tileIds;

  int tileWidth;
  int tileHeight;

  int width;
  int height;

  /// Creates a new tile layer from TMX data
  TileLayer(this.layer, this.parent) {
    width = int.parse(layer.getAttribute("width"));
    height = int.parse(layer.getAttribute("height"));

    name = layer.getAttribute("name");

    XML.XmlElement data = layer.findElements("data").first;
    if(data.getAttribute("encoding") == "csv") {
      tileIds = parseCsv(data.text);
    }

    tileWidth = parent.tileWidth;
    tileHeight = parent.tileHeight;
  }

  /// Updates the layer with the new tileset
  @override
  giveTileset(Map<int, Tile> tileset) {
    tiles = [];
    tileIds.forEach((int id) => tiles.add(parent.tileset[id - 1]));
  }

  /// Renders this to batch
  @override
  render(SpriteBatch batch, num x, num y) {
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        if (getTile(col, row) != null)
          getTile(col, row).render(batch, col * tileWidth + x,
              row * tileHeight + y, tileWidth, tileHeight);
      }
    }
  }

  /// Returns the tile at ([x], [y]) from the bottom-left
  Tile getTile(x, y) {
    //I know this function makes little sense to us mere mortals, but it works (I think)!
    return tiles[width * (height - y - 1) + x];
  }

  setTile(x, y, Tile tile) {
    tiles[width * (height - y - 1) + x] = tile;
  }

}

/// A tilemap layer filed with tiles
class DataLayer extends MapLayer {
  Tilemap parent;

  XML.XmlElement layer;

  /// Creates a new tile layer from TMX data
  DataLayer(this.layer, this.parent) {
    name = layer.getAttribute("name");
  }

  /// Updates the layer with the new tileset
  @override
  giveTileset(Map<int, Tile> tileset) {}

  /// Renders this to batch
  @override
  render(SpriteBatch batch, num x, num y) {}

}
