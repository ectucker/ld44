part of ld44;

typedef Action = int Function(
    PlayerController control, int direction, int directionalX);

class Hotbar {

  var atlas;

  int insanity = 0;

  Powerup q;
  Powerup w;
  Powerup e;

  Hotbar(this.atlas) {
    q = Powerup(atlas["empty"]);
    w = Powerup(atlas["empty"]);
    e = Powerup(atlas["empty"]);
  }

  Hotbar clone() {
    Hotbar clone = new Hotbar(atlas);
    clone.q = this.q;
    clone.w = this.w;
    clone.e = this.e;
    clone.insanity = this.insanity;
    return clone;
  }
}

class Powerup {
  Texture icon;

  WorldEntity player;
  WorldMap world;

  int xLeft;
  int xRight;
  int y;

  int height;
  int width;

  Action apply = (player, dir, x) => 0;

  Powerup(this.icon);

  Powerup.fromShrine(Shrine shrine) {
    icon = atlas[shrine.type];
    if (shrine.type == "highjump") {
      apply = (control, dir, x) {
        int ledgeHeight = world.platformHeight(x, y, dir);
        if (ledgeHeight != 0 && ledgeHeight <= 6) {
          if (world.firstTileAbove(x, y) > ledgeHeight + player.height - 1 &&
              world.firstTileAbove(xRight, y) > ledgeHeight + height - 1) {
            if (player.fitsAt(player.x + PLAYER_WIDTH * dir, player.y + ledgeHeight)) {
              player.climb(ledgeHeight, dir);
              control.stepCountdown = player.climbTime(ledgeHeight);
              control.totalStepCountdown = control.stepCountdown;
              control.targetY = player.y + ledgeHeight;
              control.maxY = 0;
              if(ledgeHeight > 2) {
                control.currentAnimation = 3;
              } else {
                control.currentAnimation = 2;
              }
              return 2; // 2 = Vertical Jump
            }
          }
        }
      };
    }
    if (shrine.type == "superdash") {
      apply = (control, dir, x) {
        int gapDist = world.gapDist(x, player.y, dir);
        if (gapDist != 0 && gapDist.abs() <= 5) {
          gapDist += PLAYER_WIDTH * dir;
          if (player.fitsAt(player.x + gapDist, player.y)) {
            player.jumpX(gapDist);
            control.stepCountdown = JUMP_TIME * gapDist.abs();
            control.totalStepCountdown = control.stepCountdown;
            control.currentAnimation = 5;
            return 1; // 1 = Horizontal Jump
          }
        }
      };
    }
    if (shrine.type == "melee") {
      apply = (control, dir, x) {
        world.entities.forEach((WorldEntity e) {
          if(e.health < 100) {
            if (dir == -1) {
              if (e.xRight < x && x - e.xRight < 3 && e.y == player.y) {
                e.health -= 4;
                damageAnimation(e);
                print("hit");
                assetManager.get("hit").play();
              }
            } else if (dir == 1) {
              if (e.x > x && e.x - x < 3 && e.y == player.y) {
                e.health -= 4;
                damageAnimation(e);
                print("hit");
                assetManager.get("hit").play();
              }
            }
          }
          control.stepCountdown = PLAYER_ATTACK_TIME;
          control.currentAnimation = 6;
        });
        assetManager.get("swing").play();
      };
    }
    if (shrine.type == "attackbetter") {
      apply = (control, dir, x) {
        world.entities.forEach((WorldEntity e) {
          if(e.health < 100) {
            if (dir == -1) {
              if (e.xRight < x && x - e.xRight < 3 && e.y == player.y) {
                e.health -= 5;
                print("hit");
                assetManager.get("hit").play();
              }
            } else if (dir == 1) {
              if (e.x > x && e.x - x < 3 && e.y == player.y) {
                e.health -= 5;
                print("hit");
                assetManager.get("hit").play();
              }
            }
          }
          control.stepCountdown = PLAYER_ATTACK_TIME;
          control.currentAnimation = 6;
        });
        assetManager.get("swing").play();
      };
    }
    if (shrine.type == "featherfall") {
      apply = (control, dir, x) {
        int pitDepth = world.pitDepth(x, player.y, dir);
        print(pitDepth);
        if (pitDepth != 0) {
          print("Valid depth");
          if (player.fitsAt(
              player.x + PLAYER_WIDTH * dir, player.y + pitDepth) &&
              player.fitsAt(player.x + PLAYER_WIDTH * dir, player.y)) {
            print("Fits");
            player.drop(pitDepth, dir);
            control.stepCountdown = JUMP_TIME * (player.width + pitDepth.abs());
            control.totalStepCountdown = control.stepCountdown;
            control.currentAnimation = 5;
            return 3; // 3 = Climb Down
          }
        }
      };
    }
    if (shrine.type == "armor") {
      apply = (control, dir, x) {
        if(player.health < 3) {
          player.health = 3;
          hotbar.insanity += 1;
          control.stepCountdown = 1.0;
        }
      };
    }
    if (shrine.type == "magicprojectile") {
      apply = (control, dir, x) {
        //TODO
      };
    }
    if (shrine.type == "vanish") {
      apply = (control, dir, x) {
        control.vanishTime = 1.5;
        control.stepCountdown = 0.2;
      };
    }
  }

  setHelpers(PlayerController control) {
    player = control.player;
    world = control.world;
    xLeft = player.x;
    xRight = player.x;
    y = player.y;
    width = player.width;
    height = player.height;
  }
}
