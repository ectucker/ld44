part of ld44;

List<TutorialEntity> listTexts(DataLayer layer, WorldMap map) {
  List<TutorialEntity> tutorial = [];
  layer.layer.findElements("object").forEach((XML.XmlElement object) {
    int x = int.parse(object.getAttribute("x")) ~/ TILE_SIZE;
    int y = (SCREEN_PIXEL_HEIGHT - TILE_SIZE * 2 - int.parse(object.getAttribute("y"))) ~/ TILE_SIZE;
    String name = object.getAttribute("name");
    TutorialEntity entity = new TutorialEntity(map, atlas["blank"], x, y, 1, 1, assetManager.get("noaura"), name);
    tutorial.add(entity);
  });
  return tutorial;
}

class TutorialEntity extends WorldEntity {

  String text;

  TutorialEntity(WorldMap map, texture, int x, int y, int width, int height, ParticleEffect effect, this.text) : super(map, texture, x, y, width, height, effect);

}

