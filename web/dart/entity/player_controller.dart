part of ld44;

Hotbar hotbar;
Hotbar oldHotbar;

class PlayerController {
  WorldMap world;

  WorldEntity player;

  Keyboard keyboard;
  Mouse mouse;

  var atlas;

  double stepCountdown = STEP_TIME;
  double totalStepCountdown;

  int direction = 1;

  bool controlled = true;

  double vanishTime = 0.0;

  // 0 = standing
  // 1 = walking
  // 2 = falling
  // 3 = jumping
  // 4 = climbing
  // 5 = leaping
  // 6 = attack
  int currentAnimation = 0;
  int lastAnim = 0;
  int animCycles = 0;
  int targetY;
  double maxY = 0;

  PlayerController(
      this.player, this.world, this.keyboard, this.mouse, this.atlas) {
    if(hotbar == null) {
      hotbar = new Hotbar(atlas);
    } else {

    }
  }

  update(double delta) {
    if(controlled) {
      stepCountdown -= delta;
      if (stepCountdown <= 0) {
        stepAbility();
      }
      if (stepCountdown <= 0) {
        lastAnim = currentAnimation;
        currentAnimation = 0;
        stepMovement();
        if(lastAnim == currentAnimation) {
          animCycles += 1;
        } else {
          animCycles = 0;
        }
      }
      if(vanishTime > 0) {
        vanishTime -= delta;
        player.vanished = true;
      } else {
        player.vanished = false;
      }
      player.drawOffsetX = 0.0;
      player.drawOffsetY = 0.0;
      switch(currentAnimation) {
        case 0:
          player.texture = atlas["StandingFrame"];
          break;
        case 1:
          int frame = animCycles % 5 + 1;
          player.texture = atlas["WalkingAnimationFrame$frame"];
          break;
        case 5:
        case 2:
          if(totalStepCountdown - stepCountdown < 0.1) {
            player.texture = atlas["JumpingFrame2"];
          } else if(totalStepCountdown - stepCountdown < 0.2) {
            player.texture = atlas["JumpingFrame3"];
          } else {
            player.texture = atlas["JumpingFrame4"];
          }
          if(stepCountdown < 0.1) {
            player.texture = atlas["JumpingFrame3"];
          } else if(stepCountdown < 0.2) {
            player.texture = atlas["JumpingFrame2"];
          }
          break;
        case 3:
          if(player.smoothY > maxY) {
            maxY = player.smoothY;
          }
          if(targetY - player.smoothY < 0.3) {
            player.texture = atlas["GrabbingFrame8"];
            player.drawOffsetX = direction * 2.0 * TILE_SIZE;
            player.drawOffsetY = -32;
          } else if(targetY - player.smoothY < 1.0) {
            player.texture = atlas["GrabbingFrame8"];
            player.drawOffsetX = direction * 1.0 * TILE_SIZE;
            player.drawOffsetY = -32;
          } else if(targetY - player.smoothY < 2) {
            player.texture = atlas["GrabbingFrame7"];
            player.drawOffsetX = 44.0 * direction;
            player.drawOffsetY = (targetY - player.smoothY) * TILE_SIZE - 59 - 32;
          } else if(targetY - player.smoothY < 2.5) {
            player.texture = atlas["GrabbingFrame6"];
            player.drawOffsetX = 44.0 * direction;
            player.drawOffsetY = (targetY - player.smoothY) * TILE_SIZE - 57 - 32;
          } else if(targetY - player.smoothY < 3) {
            player.texture = atlas["GrabbingFrame5"];
            player.drawOffsetX = 17.0 * direction;
            player.drawOffsetY = (targetY - player.smoothY) * TILE_SIZE - 104 - 32;
          } else if(targetY - player.smoothY < 3.5) {
            player.texture = atlas["GrabbingFrame4"];
            player.drawOffsetX = 7.0 * direction;
            player.drawOffsetY = (targetY - player.smoothY) * TILE_SIZE - 132 - 32;
          } else if(targetY - player.smoothY <= 4.0) {
            player.texture = atlas["GrabbingFrame3"];
            player.drawOffsetX = 0.0 * direction;
            player.drawOffsetY = -32;
          } else {
            player.texture = atlas["GrabbingFrame2"];
            player.drawOffsetY = -32;
          }
          break;
        case 6:
          if(stepCountdown < 0.2) {
            player.texture = atlas["AttackFrame4"];
          } else if(stepCountdown < 0.4) {
            player.texture = atlas["AttackFrame3"];
          } else if(stepCountdown < 0.6) {
            player.texture = atlas["AttackFrame2"];
          } else {
            player.texture = atlas["StandingFrame"];
          }
          break;
      }
      if(keyboard.keyJustPressed(KeyCode.R)) {
        controlled = false;
        game.restartLevel();
      }
    }
  }

  stepMovement() {
    if (keyboard.keyPressed(KeyCode.LEFT) ==
        keyboard.keyPressed(KeyCode.RIGHT)) {
      // Keep direction from last frame
    } else if (keyboard.keyPressed(KeyCode.LEFT)) {
      direction = -1;
    } else if (keyboard.keyPressed(KeyCode.RIGHT)) {
      direction = 1;
    }

    player.facing = direction;

    int directionalX;
    if (direction < 0) {
      directionalX = player.x;
    } else {
      directionalX = player.xRight;
    }

    // Priority 1: Jump
    if (keyboard.keyPressed(KeyCode.UP)) {
      int gapDist = world.gapDist(directionalX, player.y, direction);
      if (gapDist != 0 && gapDist.abs() <= 3) {
        gapDist += PLAYER_WIDTH * direction;
        if (player.fitsAt(player.x + gapDist, player.y)) {
          player.jumpX(gapDist);
          stepCountdown = JUMP_TIME * gapDist.abs();
          totalStepCountdown = stepCountdown;
          currentAnimation = 5;
          assetManager.get("walk").stop();
          return 1; // 1 = Horizontal Jump
        }
      }

      int ledgeHeight = world.platformHeight(directionalX, player.y, direction);
      if (ledgeHeight != 0 && ledgeHeight <= 5) {
        if (world.firstTileAbove(player.x, player.y) >
                ledgeHeight + player.height - 1 &&
            world.firstTileAbove(player.xRight, player.y) >
                ledgeHeight + player.height - 1) {
          if (player.fitsAt(
              player.x + PLAYER_WIDTH * direction, player.y + ledgeHeight)) {
            player.climb(ledgeHeight, direction);
            stepCountdown = player.climbTime(ledgeHeight);
            totalStepCountdown = stepCountdown;
            targetY = player.y + ledgeHeight;
            maxY = 0;
            if(ledgeHeight > 2) {
              currentAnimation = 3;
              assetManager.get("climb").play();
            } else {
              currentAnimation = 2;
            }
            assetManager.get("walk").stop();
            return 2; // 2 = Vertical Jump
          }
        }
      }
    }

    // Priority 2: Climb down
    if (keyboard.keyPressed(KeyCode.DOWN)) {
      int pitDepth = world.pitDepth(directionalX, player.y, direction);
      if (pitDepth != 0 && pitDepth.abs() <= 6) {
        if (player.fitsAt(
                player.x + PLAYER_WIDTH * direction, player.y + pitDepth) &&
            player.fitsAt(player.x + PLAYER_WIDTH * direction, player.y)) {
          player.drop(pitDepth, direction);
          stepCountdown = JUMP_TIME * (player.width + pitDepth.abs());
          totalStepCountdown = stepCountdown;
          currentAnimation = 2;
          assetManager.get("walk").stop();
          return 3; // 3 = Climb Down
        }
      }

      WorldEntity door = world.entities.firstWhere((e) => e.health > 1000, orElse: () => null);
      if(door != null) {
        if ((door.x == player.x || door.xRight == player.x) && door.y == player.y) {
          game.nextLevel();
          controlled = false;
        }
      }
    }

    // Priority 3: walking
    if (keyboard.keyPressed(KeyCode.LEFT) !=
        keyboard.keyPressed(KeyCode.RIGHT)) {
      if (keyboard.keyPressed(KeyCode.LEFT)) {
        if (!player.stepBlocked(-1)) {
          player.step(-1);
          stepCountdown = STEP_TIME;
          currentAnimation = 1;
          assetManager.get("walk").playIfNot();
          return 4; // 4 = Step
        }
      }
      if (keyboard.keyPressed(KeyCode.RIGHT)) {
        if (!player.stepBlocked(1)) {
          player.step(1);
          stepCountdown = STEP_TIME;
          currentAnimation = 1;
          assetManager.get("walk").playIfNot();
          return 4; // 4 = Step
        }
      }
    }

    assetManager.get("walk").stop();
    return 0; // 0 = No Movement
  }

  stepAbility() {
    if (keyboard.keyPressed(KeyCode.LEFT) ==
        keyboard.keyPressed(KeyCode.RIGHT)) {
      // Keep direction from last frame
    } else if (keyboard.keyPressed(KeyCode.LEFT)) {
      direction = -1;
    } else if (keyboard.keyPressed(KeyCode.RIGHT)) {
      direction = 1;
    }

    int directionalX;
    if (direction < 0) {
      directionalX = player.x;
    } else {
      directionalX = player.xRight;
    }

    if (world.currentShrine != null) {
      if (keyboard.keyPressed(KeyCode.Q)) {
        hotbar.q = Powerup.fromShrine(world.currentShrine);
        hotbar.insanity++;
        removeShrine(world.currentShrine);
        world.currentShrine.tileX = -20;
        stepCountdown = 0.5;
        assetManager.get("powerup").play();
      } else if (keyboard.keyPressed(KeyCode.W)) {
        hotbar.w = Powerup.fromShrine(world.currentShrine);
        hotbar.insanity++;
        removeShrine(world.currentShrine);
        world.currentShrine.tileX = -20;
        stepCountdown = 0.5;
        assetManager.get("powerup").play();
      } else if (keyboard.keyPressed(KeyCode.E)) {
        hotbar.e = Powerup.fromShrine(world.currentShrine);
        hotbar.insanity++;
        removeShrine(world.currentShrine);
        world.currentShrine.tileX = -20;
        stepCountdown = 0.5;
        assetManager.get("powerup").play();
      }
    } else {
      if (keyboard.keyPressed(KeyCode.Q)) {
        hotbar.q.setHelpers(this);
        hotbar.q.apply(this, direction, directionalX);
      } else if (keyboard.keyPressed(KeyCode.W)) {
        hotbar.w.setHelpers(this);
        hotbar.w.apply(this, direction, directionalX);
      } else if (keyboard.keyPressed(KeyCode.E)) {
        hotbar.e.setHelpers(this);
        hotbar.e.apply(this, direction, directionalX);
      }
    }
  }


  removeShrine(Shrine shrine) {
    try {
      TileLayer layer = world.tilemap
          .getLayersContaining("Foreground")
          .first;
      layer.setTile(
          shrine.tileX, shrine.tileY + 1, world.tilemap.tileset[NO_TOME_ID]);
    } catch (e) {

    }
  }
}
