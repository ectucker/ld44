part of ld44;

List<Enemy> listEnemies(DataLayer layer, WorldMap map) {
  List<Enemy> enemies = [];
  layer.layer.findElements("object").forEach((XML.XmlElement object) {
    int x = int.parse(object.getAttribute("x")) ~/ TILE_SIZE;
    int y = (SCREEN_PIXEL_HEIGHT -
            TILE_SIZE * 2 -
            int.parse(object.getAttribute("y"))) ~/
        TILE_SIZE;
    String name = object.getAttribute("name");
    ParticleEffect effect;
    Texture texture;
    if (name == "ground") {
      effect = assetManager.get("enemyaura");
      texture = atlas["bad1"];
    } else {
      effect = assetManager.get("noaura");
      texture = atlas["blank"];
    }
    Enemy enemy = new Enemy(map, texture, x, y, 2, 2, effect);
    if (name == "ground") {
      enemy.controller = new GroundController(enemy, map);
    } else if (name == "exit") {
      enemy.controller = new ExitController(enemy, map);
      enemy.health = 10000;
    }
    enemies.add(enemy);
  });
  return enemies;
}

class EnemyController {
  WorldMap world;

  WorldEntity player;

  Enemy enemy;

  EnemyController(this.enemy, this.world) {}

  update(double delta) {}
}

class GroundController extends EnemyController {
  int direction = 1;

  double stepTime = 0;

  bool attacking = false;

  GroundController(Enemy enemy, WorldMap world) : super(enemy, world);

  @override
  update(double delta) {
    stepTime -= delta;
    if (stepTime < 0) {
      attacking = false;
      if (enemy.player != null) {
        player = enemy.player;
        int playerDist = (player.xLeft - enemy.xLeft).abs();
        if (enemy.player.y == enemy.y && playerDist < 8 && playerDist > 2 && !player.vanished) {
          direction = (enemy.player.x - enemy.x).sign;
          enemy.step(direction);
          stepTime = GROUND_STEP_TIME;
        } else if (enemy.player.y == enemy.y && playerDist <= 2 && !player.vanished) {
          direction = (enemy.player.x - enemy.x).sign;
          assetManager.get("enemyattack").play();
          new Tween()
            ..delay = 0.7
            ..callback = () {
              int playerDist = (player.xLeft - enemy.xLeft).abs();
              if (playerDist <= 2 && (player.x - enemy.x).sign == direction && enemy.health > 0 && player.y == enemy.y) {
                player.health -= 1;
                damageAnimation(player);
              }
            }
            ..start(tweenManager);
          attacking = true;
          if (player.health <= 0) {
            print("Game Over");
          }
          stepTime = ENEMY_ATTACK_TIME;
        } else {
          if (enemy.stepBlocked(direction)) {
            direction *= -1;
          }
          enemy.step(direction);
          stepTime = GROUND_STEP_TIME;
        }
        enemy.facing = direction;
      }
    }
    if(attacking) {
      if(stepTime < 0.8) {
        enemy.texture = atlas["bad1"];
      }
      if(stepTime < 0.6) {
        enemy.texture = atlas["bad2"];
      }
      if(stepTime < 0.4) {
        enemy.texture = atlas["bad3"];
      }
      if(stepTime < 0.2) {
        enemy.texture = atlas["bad4"];
      }
      if(stepTime < 0.1) {
        enemy.texture = atlas["bad2"];
      }
    } else {
      enemy.texture = atlas["bad1"];
    }
  }
}

class ExitController extends EnemyController {
  int direction = 1;

  double stepTime = 0;

  ExitController(Enemy enemy, WorldMap world) : super(enemy, world);

  @override
  update(double delta) {}
}
