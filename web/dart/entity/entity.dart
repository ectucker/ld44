part of ld44;

class WorldEntity {
  WorldMap map;

  Texture texture;
  ParticleEmitter aura;
  Vector4 tint = Colors.white;

  int x;
  int y;

  int width;
  int height;

  int get xLeft => x;
  int get xRight => x + width - 1;

  int get yBottom => y;
  int get yTop => y + height - 1;

  double smoothX;
  double smoothY;

  double drawOffsetX = 0;
  double drawOffsetY = 0;

  int health = 3;

  int facing = 1;

  bool vanished = false;

  WorldEntity(this.map, this.texture, this.x, this.y, this.width, this.height,
      ParticleEffect effect) {
    smoothX = x.toDouble();
    smoothY = y.toDouble();
    aura = new ParticleEmitter(effect);
  }

  bool fitsAt(int x, int y) {
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        if (map.isSolid(x + i, y + j)) {
          return false;
        }
      }
    }
    return true;
  }

  bool stepBlocked(int dx) {
    bool blocked = false;
    if (!fitsAt(x + dx, y)) {
      blocked = true;
    } else if (dx > 0 && !map.isSolid(xRight + dx, y - 1)) {
      blocked = true;
    } else if (dx < 0 && !map.isSolid(xLeft + dx, y - 1)) {
      blocked = true;
    }
    return blocked;
  }

  step(int dx) {
    if (!stepBlocked(dx)) {
      new Tween()
        ..get = [() => smoothX, () => smoothY]
        ..set = [(x) => smoothX = x, (y) => smoothY = y]
        ..target = [x + dx, y]
        ..duration = STEP_TIME
        ..start(tweenManager);
      setPosIn(x + dx, y, STEP_TIME);
    }
  }

  jumpX(int dx) {
    new Tween()
      ..get = [() => smoothX, () => smoothY]
      ..set = [(x) => smoothX = x, (y) => smoothY = y]
      ..target = [x + dx, y]
      ..duration = JUMP_TIME * dx.abs()
      ..start(tweenManager);
    setPosIn(x + dx, y, JUMP_TIME * dx.abs());
  }

  climb(int height, int direction) {
    y += 1;
    if (height > 3) {
      new Tween()
        ..get = [() => smoothY]
        ..set = [(y) => smoothY = y]
        ..target = [y + height - 3 - 1]
        ..duration = 0.05 * (height.abs() - 3)
        ..ease = sineOut
        ..chain(new Tween()
          ..get = [() => smoothY]
          ..set = [(y) => smoothY = y]
          ..target = [y + height - 1 - 1]
          ..duration = CLIMB_TIME * (height.abs() - 1)
          ..ease = linearInOut
          ..chain(new Tween()
            ..get = [() => smoothY]
            ..set = [(y) => smoothY = y]
            ..target = [y + height - 1]
            ..duration = 0.05 * 1
            ..ease = quintOut))
        ..start(tweenManager);
    } else if (height > 2) {
      new Tween()
        ..get = [() => smoothY]
        ..set = [(y) => smoothY = y]
        ..target = [y + height - 1 - 1]
        ..duration = CLIMB_TIME * (height.abs() - 1)
        ..ease = linearInOut
        ..chain(new Tween()
          ..get = [() => smoothY]
          ..set = [(y) => smoothY = y]
          ..target = [y + height - 1]
          ..duration = 0.05 * 1
          ..ease = quintOut)
        ..start(tweenManager);
    } else {
      new Tween()
        ..get = [() => smoothY]
        ..set = [(y) => smoothY = y]
        ..target = [y + height - 1]
        ..duration = JUMP_TIME * height.abs()
        ..chain(new Tween()
          ..get = [() => smoothX]
          ..set = [(x) => smoothX = x]
          ..target = [x + direction * width]
          ..duration = JUMP_TIME * width)
        ..start(tweenManager);
    }
    setPosIn(x + direction * width, y + height - 1, climbTime(height));
  }

  double climbTime(int height) {
    if (height > 3) {
      return 0.05 * (height.abs() - 3) + CLIMB_TIME * (height.abs() - 1) + 0.05 * 1;
    } if (height > 2) {
      return CLIMB_TIME * (height.abs() - 1) + 0.05 * 1;
    } else {
      return JUMP_TIME * (height.abs() + width);
    }
  }

  drop(int height, int direction) {
    new Tween()
      ..get = [() => smoothX]
      ..set = [(x) => smoothX = x]
      ..target = [x + direction * width]
      ..duration = JUMP_TIME * width
      ..start(tweenManager);
    new Tween()
      ..get = [() => smoothY]
      ..set = [(y) => smoothY = y]
      ..target = [y + height]
      ..duration = JUMP_TIME * height.abs()
      ..delay = JUMP_TIME * width
      ..start(tweenManager);
    setPosIn(x + direction * width, y + height,
        JUMP_TIME * height.abs() + JUMP_TIME * width);
  }

  update(double delta) {
    aura.pos = Vector2(smoothX + 0.5, smoothY + 0.5) * TILE_SIZE.toDouble();
    aura.update(delta);
  }

  setPosIn(int newX, int newY, double time) {
    new Tween()
      ..delay = time
      ..callback = () {
        this.x = newX;
        this.y = newY;
        this.smoothX = newX.toDouble();
        this.smoothY = newY.toDouble();
      }
      ..start(tweenManager);
  }
}

class Enemy extends WorldEntity {
  EnemyController controller;

  WorldEntity player;

  Enemy(WorldMap map, texture, int x, int y, int width, int height,
      ParticleEffect effect)
      : super(map, texture, x, y, width, height, effect) {
    health = 10;
  }

  @override
  update(double delta) {
    super.update(delta);
    if (controller != null) {
      controller.update(delta);
    }
  }

  @override
  step(int dx) {
    if (!stepBlocked(dx)) {
      x += dx;
      new Tween()
        ..get = [() => smoothX, () => smoothY]
        ..set = [(x) => smoothX = x, (y) => smoothY = y]
        ..target = [x, y]
        ..duration = GROUND_STEP_TIME
        ..start(tweenManager);
    }
  }
}

damageAnimation(WorldEntity e) {
  print("flash");
  Vector4 target = Colors.red;
  new Tween()
    ..get = [() => e.tint.r, () => e.tint.g, () => e.tint.b]
    ..set = [(r) => e.tint.r = r, (g) => e.tint.g = g, (b) => e.tint.b = b]
    ..target = [target.r, target.g, target.b]
    ..duration = 0.2
    ..ease = circInOut
    ..chain(new Tween()
      ..get = [() => e.tint.r, () => e.tint.g, () => e.tint.b]
      ..set = [(r) => e.tint.r = r, (g) => e.tint.g = g, (b) => e.tint.b = b]
      ..target = [1.0, 1.0, 1.0]
      ..duration = 0.2
      ..ease = circInOut)
    ..start(tweenManager);
}
