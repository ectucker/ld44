part of ld44;

int TILE_SIZE = 32;

int SCREEN_TILE_HEIGHT = 30;

int SCREEN_PIXEL_HEIGHT = TILE_SIZE * SCREEN_TILE_HEIGHT;

int PLAYER_WIDTH = 2;
int PLAYER_HEIGHT = 4;

double STEP_TIME = 0.1;
double CLIMB_TIME = 0.25;

double JUMP_TIME = 0.08;

const int SWITCH_ID = 745;
const int DEFAULT_ON_ID = 94;
const int DEFAULT_OFF_ID = 106;
const int ON_ID = 94;
const int NO_TOME_ID = 1768;

const double GROUND_STEP_TIME = 0.4;
const double ENEMY_ATTACK_TIME = 0.8;
const double PLAYER_ATTACK_TIME = 0.5;

const Map<String, String> tomes = {
  "armor": "Tome of Dark Healing",
  "attackbetter": "Tome of Force",
  "melee": "Tome of Slash",
  "featherfall": "Tome of Featherfall",
  "highjump": "Tome of High Reach",
  "magicprojectile": "Tome of Dark Projectile",
  "superdash": "Tome of Leaping",
  "vanish": "Tome of Vanishing"
};

const Map<int, List<int>> startPos = {
  0: [4, 4],
  1: [4, 4],
  2: [4, 4],
  3: [4, 22]
};

int LAST_LEVEL = 3;

const Map<int, double> backgroundMoveRates = {
  1: 1,
  2: 0.6,
  3: 0.4
};