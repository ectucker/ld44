precision highp float;

uniform sampler2D uTexture;
uniform vec4 uLightColor;

varying vec2 vTextureCoord;
varying vec4 vColor;

void main(void) {
    gl_FragColor = texture2D(uTexture, vec2(vTextureCoord.s, vTextureCoord.t)) * vColor;
    if(gl_FragColor.a <= 0.2)
        discard;
    gl_FragColor.a = 1.0;
}