precision highp float;

uniform sampler2D uTexture;
uniform float uSdfScale;

varying vec2 vTextureCoord;
varying vec4 vColor;

void main() {
    float distance = texture2D(uTexture, vTextureCoord).a;
    float smoothing = 0.25 / (uSdfScale * 4.0);
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    gl_FragColor = vec4(vec3(1.0, 1.0, 1.0), vColor.a * alpha);
}