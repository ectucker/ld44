precision highp float;

 uniform sampler2D uTexture;
 uniform float uPercent;

 varying vec2 vTextureCoord;
 varying vec4 vColor;

 void main(void) {
     vec4 map = texture2D(uTexture, vec2(vTextureCoord.s, vTextureCoord.t)) * vColor;

     if(map.b < uPercent)
         gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
 }